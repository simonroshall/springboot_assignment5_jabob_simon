# 👏 Assignment 5 Springboot 

This is assignment 5 for Experis Academy, it is made in Java, using Springboot and Postgres.

### 🔥 Background

The assignment is divided into two parts. The first part consists of a number of SQL-scripts which can be found in the folder SQL-Scripts. By running the scripts, four tables is created with some test data. 
The second part of the assignment includes a java project where we execute a number of SQL-scripts towards Postgres. We were provided the database from Noroff Accelerate. All the scripts are commented out in CustomerRunner. 

### 🤖 Technologies

* Java jdk-17
* Intellij Idea
* Springboot
* Gradle
* Postgres

### ⚠️Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

To get the application running, simply clone the repo below: 
```
git clone https://gitlab.com/simonroshall/springboot_assignment5_jabob_simon
```

### 💻 Executing program

How to run the application:
```
* Open IntelliJ and use Run or Shift+F10 on the 'Assignment5SpringBootApplication'
```
### 😎 Contributors

Contributors and contact information

Jacob Andersson
[@Jacob_A](https://gitlab.com/Jacob_A/)

Simon Roshäll 
[@simonroshall](https://gitlab.com/simonroshall/)


### 🌌 Contributing

PRs are not accepted

## 🦝 Version History

* 0.1
  * Initial Release
