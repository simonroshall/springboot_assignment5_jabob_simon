package experis.assignment5_springboot.models;

// Model for Customer, contains the fields for a customer in the DB
public record Customer(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email) {
}
