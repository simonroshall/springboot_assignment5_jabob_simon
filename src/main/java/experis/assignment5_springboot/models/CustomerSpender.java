package experis.assignment5_springboot.models;

// Model for CustomerSpender, (used to fetch the name of the customer with the highest total amount spent)
public record CustomerSpender(String firstName) {
}
