package experis.assignment5_springboot.models;

// Model for CustomerCountry (used to fetch the country with the highest total amount of customer)
public record CustomerCountry(String country) {
}
