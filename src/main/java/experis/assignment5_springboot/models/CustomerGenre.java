package experis.assignment5_springboot.models;

// Model for CustomerGenre (used to fetch the genre with the highest amount of listing)
public record CustomerGenre(String genre) {
}
