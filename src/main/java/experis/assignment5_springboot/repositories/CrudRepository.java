package experis.assignment5_springboot.repositories;

import java.util.List;

// CRUD interface with standard method declaration
public interface CrudRepository<T, ID> {
    List<T> findAll();
    T findById(ID id);
    int insert(T object);
    int update(T object);
    int delete(T object);
    int deleteById(ID id);
}