package experis.assignment5_springboot.repositories.customer;

import experis.assignment5_springboot.models.Customer;
import experis.assignment5_springboot.models.CustomerCountry;
import experis.assignment5_springboot.models.CustomerGenre;
import experis.assignment5_springboot.models.CustomerSpender;
import experis.assignment5_springboot.repositories.CrudRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// Repository with implemented methods
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private String url;
    private String username;
    private String password;

    // Fetches the value from the application.properties file
    // Sets up the connection to the database
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    // Fetches all customer from the database and returns the list of users found
    @Override
    public List<Customer> findAll() {
        List<Customer> customer = new ArrayList<>();
        String sql = "SELECT * FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Prepare statement, cleans the input to prevent SQL-injections
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            // Execute the query (query since we are doing any insert/update/delete)
            // and return the response from the DB
            ResultSet resultSet = preparedStatement.executeQuery();
            // Fetches from the DB until the results "runs out"
            while (resultSet.next()) {
                // Creates a customer for each results
                customer.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Return the list of customers
        return customer;
    }

    // Fetch specific user by their id from the DB
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            // Injects the id provided into the query
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Return the specific person fetched from the DB
        return customer;
    }

    // Insert new entry into DB
    @Override
    public int insert(Customer customer) {
        var sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        int affectedRows = 0;
        // Set up all the data to be inserted into the DB
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());
            // Execute update since we are doing an insert
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Return the number of affected rows, should be 1 in this case
        return affectedRows;
    }

    // Update existing entry in DB by their id
    @Override
    public int update(Customer customer) {
        var sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        int affectedRows = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());
            preparedStatement.setInt(7, customer.id());
            affectedRows = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns number of affected rows after the update query
        return affectedRows;
    }

    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Integer integer) {
        return 0;
    }

    // Fetch user from the DB by their name
    @Override
    public Customer findByName(String name) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns the specific customer object
        return customer;
    }

    // Finds a set of users with a limit and offset within the DB
    @Override
    public List<Customer> findByLimitAndOffset(int limit, int offset) {
        List<Customer> customer = new ArrayList<>();
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer.add(new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns list of customers fetched from DB
        return customer;
    }

    // Fetches the country with the most entries in the DB
    @Override
    public String findCountryWithMostCustomers() {
        // Initialize record customer country
        CustomerCountry customerCountry = null;
        var sql = "SELECT MAX(country) AS mostCommonCountry FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerCountry = new CustomerCountry(
                        resultSet.getString("mostCommonCountry")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns the country as a string
        return customerCountry.country();
    }

    // Fetches the name of the spender with the highest sum of purchases
    @Override
    public String findCustomerHighestSpender() {
        CustomerSpender customerSpender = null;
        // Uses inner join to take the invoice list (contains the FK customer_id) and join with the customer list
        // Fetches the first entry (multiple if tied) of the list and returns the name
        var sql = "SELECT first_name, SUM(invoice.total) AS total FROM customer INNER JOIN invoice ON invoice.customer_id = customer.customer_id GROUP BY customer.customer_id ORDER BY total DESC FETCH FIRST 1 ROW WITH TIES";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerSpender = new CustomerSpender(
                        resultSet.getString("first_name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns the name of the spender as a string
        return customerSpender.firstName();
    }

    // Fetches the name of the most popular genre of music by most purchases.
    @Override
    public List<CustomerGenre> findMostPopularGenre() {
        List<CustomerGenre> customerGenre = new ArrayList<>();
        var sql = "SELECT genre.name, COUNT(genre.name) AS MostCommon FROM customer INNER JOIN invoice ON  invoice.customer_id = customer.customer_id INNER JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id INNER JOIN track ON track.track_id = invoice_line.track_id INNER JOIN genre ON genre.genre_id = track.genre_id WHERE customer.customer_id = 12 GROUP BY genre.name ORDER BY MostCommon desc FETCH FIRST 1 ROW WITH TIES";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerGenre.add(new CustomerGenre(
                        resultSet.getString("name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Returns the list of genres
        return customerGenre;
    }
}