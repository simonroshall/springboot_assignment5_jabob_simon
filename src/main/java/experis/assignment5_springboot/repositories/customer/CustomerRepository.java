package experis.assignment5_springboot.repositories.customer;

import experis.assignment5_springboot.models.Customer;
import experis.assignment5_springboot.models.CustomerGenre;
import experis.assignment5_springboot.repositories.CrudRepository;

import java.util.List;

// Interface for specific method declaration for customer
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findByName(String name);
    List<Customer> findByLimitAndOffset(int limit, int offset);
    String findCountryWithMostCustomers();
    String findCustomerHighestSpender();
    List<CustomerGenre> findMostPopularGenre();
}
