package experis.assignment5_springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment5SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment5SpringbootApplication.class, args);
    }

}
