package experis.assignment5_springboot.runners;

import experis.assignment5_springboot.models.Customer;
import experis.assignment5_springboot.repositories.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /* Tests for all cases described in Appendix B */
        /* Ordered from 1 to 9, uncomment these and run the program */

        /*
        System.out.println(customerRepository.findAll());
        System.out.println(customerRepository.findById(1));
        System.out.println(customerRepository.findByName("Frank"));
        System.out.println(customerRepository.findByLimitAndOffset(5,5));
        System.out.println(customerRepository.insert(new Customer(0, "Jacob", "Andersson", "Sweden", "12345", "070123456789", "Jacob.andersson@mail.com")));
        System.out.println(customerRepository.update(new Customer(0, "Jacob", "Andersson", "Sweden", "54321", "070123456789", "Jacob.andersson@mail.com")));
        System.out.println(customerRepository.findCountryWithMostCustomers());
        System.out.println(customerRepository.findCustomerHighestSpender());
        System.out.println(customerRepository.findMostPopularGenre());
        */
    }
}