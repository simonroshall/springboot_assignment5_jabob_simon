INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Bruce','Batman','Cave');

INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Clark','Superman','Space');

INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Tony','Ironman','Earth');