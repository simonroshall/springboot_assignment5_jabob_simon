INSERT INTO "power" (power_name, power_description)
VALUES ('Flying','Flying');

INSERT INTO "power" (power_name, power_description)
VALUES ('Fighting','Fighting the enemy');

INSERT INTO "power" (power_name, power_description)
VALUES ('Eye laser','throws laser');

INSERT INTO "power" (power_name, power_description)
VALUES ('Strength','Very very strong');

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (2,1);

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (2,2);

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (2,3);

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (2,4);

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (1,1);

INSERT INTO superhero_power (superhero_id, power_id)
VALUES (3,1);