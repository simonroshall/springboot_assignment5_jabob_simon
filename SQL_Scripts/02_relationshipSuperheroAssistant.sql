ALTER TABLE assistant
ADD COLUMN superhero_id int,
ADD FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id)