CREATE TABLE superhero (
    superhero_id SERIAL PRIMARY KEY,
    superhero_name varchar(50) NOT NULL,
    superhero_alias varchar(50),
    superhero_origin varchar(50)
);

CREATE TABLE assistant (
    assistant_id SERIAL PRIMARY KEY,
    assistant_name varchar(50) NOT NULL
);

CREATE TABLE "power" (
    power_id SERIAL PRIMARY KEY,
    power_name varchar(50) NOT NULL,
    power_description varchar(50)
);